require 'rubygems'
require 'mqtt'
require 'json'
require 'base64'
require 'net/http'
require 'uri'
require 'timeout'
require 'telegram/bot'


$stdout.reopen('/tmp/mydaemon.log', 'a')
$stdout.sync = true

class Watering
  TELEGRAM_TOKEN = '838437714:AAGm47YsWDw9guGTiwgURb5fZz2F0rYhhao'
  TELEGRAM_ID = 209446525
  DURATION = 3600
  PIN = 13
  TOGGLE = 1
  IP = "10.0.0.40"
  APP_ID = 'elsterweg3'
  ACCESS_KEY = 'ttn-account-v2.MfauYXrdoM6cGC7mFlq3d5E_fah_TeEIF8hcp3brIkM'
  # if above threshold, we want to water
  WATERING_THRESHOLD = 70
  WATERING_BETWEEN = 7..9

  def initialize
    send_telegram_message("Starting process")  
    @watering = false
    threads = []
    threads << Thread.new do init_telegram() end
    threads << Thread.new do listen() end
    #init_telegram
    threads.map(&:join) 
  end

  def listen
    puts "Starting to listen"
    url = "mqtt://#{APP_ID}:#{ACCESS_KEY}@eu.thethings.network:1883"
    puts url
      MQTT::Client.connect(url) do |c|
        puts "Connected"
        # If you pass a block to the get method, then it will loop
        c.get("#{APP_ID}/devices/+/up") do |topic, message|
          puts "#{topic}: #{message}"

          payload = JSON.parse(message)
          data = payload['payload_fields']

          @humidity_value = data["humidity"]
          @humidity_time = "#{@humidity_value}: #{Time.now}"
          puts "HUMIDITY #{@humidity_time}"
          puts "HUMIDITY #{@humidity_value}"

          if currently_watering?
            puts "currently watering"
            next
          end

          if !watering_time
            puts "not watering time"
            next
          end  

          if @humidity_value > WATERING_THRESHOLD
            send_telegram_message "Watering: humidity value above threshold of #{WATERING_THRESHOLD}, value = #{@humidity_value}"
            begin
              start_watering
            rescue
              puts "crashed"
            end
          else
            puts "Not Watering: humidity below threshold of #{WATERING_THRESHOLD}, value = #{@humidity_value}"
          end
      end      
    end
  end

  def start_watering(duration = nil)
    watering_time = duration || DURATION
    puts "starting watering for #{watering_time}"
    uri = URI("http://#{IP}/control?cmd=LongPulse,#{PIN},#{TOGGLE},#{watering_time}")
    begin
      Net::HTTP.start(uri.host, uri.port) do |http|
        request = Net::HTTP::Get.new uri.request_uri

        response = http.request request # Net::HTTPResponse object
        
        @watering = true
        time_watering_reset(watering_time)                
        send_telegram_message("Started watering for #{watering_time} seconds")
        puts response.inspect
      end

    rescue
      @watering = false
      puts "ESP32 not available"
      send_telegram_message "ESP32 not available"
    end  
    # Net::HTTP.get(IP, "/control?cmd=LongPulse,#{PIN},#{TOGGLE},#{DURATION}")
  end

  def stop_watering
    Net::HTTP.get(IP, "/control?cmd=LongPulse,#{PIN},#{TOGGLE},#{1}")
    time_watering_reset(1)   
  end

  def notify_slack(msg)
    uri = URI.parse("https://hooks.slack.com/services/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")

    header = { 'Content-Type' => 'text/json' }
    data = {"text" => msg}

    # Create the HTTP objects
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = data.to_json

    # Send the request
    http.request(request)
  end

  # Only water in the early hours
  def watering_time
    WATERING_BETWEEN.cover?(Time.now.hour)    
  end

  def currently_watering?
    @watering
  end

  def time_watering_reset(duration)
    Timeout::timeout(duration) do
      puts "Watering finished"
      send_telegram_message "Watering finished"
      @watering = false
    end
  end  

  def send_telegram_message(msg)
    Telegram::Bot::Client.run(TELEGRAM_TOKEN) do |bot|
      bot.api.send_message(chat_id: TELEGRAM_ID, text: msg)
    end
  end  

  def init_telegram()
    puts "initializing telegram bot"    
    Telegram::Bot::Client.run(TELEGRAM_TOKEN) do |bot|
      bot.listen do |message|
        case message.text
        when '/start'
          @chat_id = message.chat.id
          puts @chat_id
          send_telegram_message("Hello, #{message.from.first_name}")
        when /start_watering (\d+)/
          start_watering($1)
          send_telegram_message($1)
        when '/start_watering'
          start_watering
          send_telegram_message("Started watering")
        when '/stop_watering'
          @watering = false
          stop_watering
          send_telegram_message("Stopping watering")
        when '/reset_watering'
          @watering = false
          send_telegram_message("Watering: #{@watering}")
        when '/humidity'
          send_telegram_message("Humidity: #{@humidity_time}")
        end
      end
    end    
  end 
end

Watering.new

